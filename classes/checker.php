<?php
/**
 * @package pendingContentCheck
 * @class   PendingContentChecker
 * @author  Serhey Dolgushev <dolgushev.serhey@gmail.com>
 * @date    04 Oct 2013
 **/

class PendingContentChecker
{
	const WARNINGS_SESSION_VAR = 'pending_content_warnings';

	static public function check( $uri ) {
		if( strpos( $uri->attribute( 'uri' ), 'content/edit' ) === false ) {
			return true;
		}

		$object = eZContentObject::fetch( $uri->element( 2, false ) );
		if( $object instanceof eZContentObject === false ) {
			return true;
		}

		$params = array();
		if( $uri->element( 4, false ) !== null ) {
			$params = array(
				'conditions' => array(
					'language_code' => $uri->element( 4, false )
				)
			);
		}
		$versions = $object->versions( true, $params );

		$pendingVersions = array();
		$currentVersion  = $uri->element( 3, false );
		foreach( $versions as $version ) {
			if( $version->attribute( 'version' ) == $currentVersion ) {
				continue;
			}

			if( (int) $version->attribute( 'status' ) === eZContentObjectVersion::STATUS_PENDING ) {
				$pendingVersions[] = $version;
			}
		}

		if( count( $pendingVersions ) > 0 ) {
			$tpl = eZTemplate::factory();
			$tpl->setVariable( 'pending_versions', $pendingVersions );
			$warning = $tpl->fetch( 'design:pending_content_warning.tpl' );
			eZHTTPTool::instance()->setSessionVariable( self::WARNINGS_SESSION_VAR, $warning );
		}
	}

	static public function showWarning( $output ) {
		$http    = eZHTTPTool::instance();
		$warning = $http->sessionVariable( self::WARNINGS_SESSION_VAR, null );
		if( $warning !== null ) {
			$output = str_replace( '</body>', $warning . "\n" . '</body>', $output );
			$http->removeSessionVariable( self::WARNINGS_SESSION_VAR );
		}

		return $output;
	}
}