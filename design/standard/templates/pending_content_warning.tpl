<div class="pending-content-warning">
	{literal}
		<style type = "text/css" scoped>
			div.pending-content-warning {
				border: 3px solid red;
				position: fixed;
				z-index: 9999;
				top: 150px;
				width: 90%;
				background: white;
				margin: 0px 5%;
			}

			div.pending-content-warning div {
				width: 96%;
				padding: 10px 2%;
			}
			a.close-notification {
				display: block;
				float: right;
				margin: 10px 0px;
			}
		</style>
	{/literal}

	<div>
		{'There are pending content versions'|i18n( 'extension/nxc_pending_content_check' )}:
		<ul>
			{foreach $pending_versions as $version}
				<li>{$version.name|wash}, {$version.version}, {$version.creator.name|wash}</li>
			{/foreach}
		</ul>

		<a class="close-notification" href="#" onclick="var e = this.parentNode.parentNode; e.parentNode.removeChild( e ); return true;">{'Close'|i18n( 'extension/nxc_pending_content_check' )}</a>
	</div>
</div>